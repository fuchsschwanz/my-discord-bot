import discord
import asyncio
import newgenerator as ng
from discord.ext import commands

import os, random

TOKEN = 'your token'

DIRECTORY = "your directory"
imagelist = os.listdir(DIRECTORY)

description = 'Just playing around with new stuff'


class MyBot(commands.Cog):

    @commands.command()
    async def pic(self, ctx):
        rand_img = random.choice(imagelist)
        msg = 'Here comes your pic, {0.author.mention}'.format(ctx)
        with open(DIRECTORY + rand_img, 'rb') as f:
            await ctx.send(msg, file=discord.File(f))

    @commands.command()
    async def hello(self, ctx):
        await ctx.send('Hello {0.author.mention}'.format(ctx))

    @commands.command()
    async def av(self, ctx, member: discord.Member):
        await ctx.send(member.avatar_url)

    @commands.command()
    async def joined(self, ctx, member: discord.Member):
        await ctx.send('{0.name} joined in {0.joined_at}'.format(member))

    @commands.command()
    async def gen(self, ctx, number: int):
        sep = ", "
        nicks = []

        if 20 >= number >= 1:
            for x in range(number):
                nick_gen = ng.nowywybor()
                nicks.append(nick_gen)
            msg = "Here are random generated nicknames for you: `" + \
                  sep.join(nicks) + "`."
        else:
            msg = "Type !gen number, where number is from 1 to 20"

        await ctx.send(msg)


bot = commands.Bot(command_prefix='!', description=description)


@bot.listen("on_command_error")
async def handle_error(ctx, exc):
    if isinstance(exc, commands.MissingRequiredArgument):
        await ctx.send("Please specify an argument")

@bot.event
async def on_ready():
    game = discord.Game("Death Stranding")
    await bot.change_presence(activity=game)
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('-----------')

bot.add_cog(MyBot(bot))
bot.run(TOKEN)
