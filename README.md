# Discord bot python

Simple discord bot wrote in discord.py.
I made it for fun, and rewrote it using newer discord.py version.

Here is more info about it: https://discordpy.readthedocs.io/en/latest/migrating.html

I made it just for fun and for my own purpose, but if you want to use it, you have to modify TOKEN value to be you bot's token.
You will also need to change DIRECTORY to be a path to images folder.

Available commands:

- !hello - bot will mention and greet you
- !joined [mention user] - bot will show when an user joined this server
- !av [mention user] - bot will post profile picture of mentioned user
- !pic - bot will post randomly choosen image from DIRECTORY
- !gen [1-20] - bot will generate from 1 to 20 nickname ideas. It can be... helpful... I think.