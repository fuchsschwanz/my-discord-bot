import random
import itertools

nowelitery = [
        ('a', 8.91), ('i', 8.21), ('o', 7.75), 
        ('u', 2.5), ('e', 7.66), ('m', 2.8), 
        ('d', 3.25), ('n', 5.52), ('r', 4.69), 
        ('z', 5.64), ('l', 2.1), ('j', 2.28), 
        ('g', 1.42), ('w', 4.65), ('p', 3.13), 
        ('f', 0.3), ('t', 3.98), ('s', 4.32), 
        ('k', 3.51), ('x', 0.02), ('y', 3.76)
        ]

nowesp = [
    ('m', 2.8), ('d', 3.25), ('n', 5.52), 
    ('r', 4.69), ('z', 5.64), ('l', 2.1), 
    ('j', 2.28), ('g', 1.42), ('w', 4.65), 
    ('p', 3.13), ('f', 0.3), ('t', 3.98), 
    ('s', 4.32), ('k', 3.51), ('x', 0.02)
    ]

nowesam = [
    ('a', 8.91), ('i', 8.21), ('o', 7.75), 
    ('u', 2.5), ('e', 7.66), ('y', 3.76)
    ]


def nowylos(rzecz):
    g = 0
    totals = []
    for i in list(zip(*rzecz))[1]:
        g += i
        totals.append(g)

    rnd = random.random() * g
    for i, total in enumerate(totals):
        if rnd < total:
            x = rzecz[i][0]
            return x
            break  


def nowywybor():
    licznik = 0
    slowo = nowylos(nowelitery)
    licznikowa = random.randint(1, 3)
    slowna = random.randint(3, 11)
    
    xa = list(zip(*nowesam))[0]
    xb = list(zip(*nowesp))[0]

    for x in range(slowna):
        if licznik == licznikowa:
            slowo += nowylos(nowesam)
            licznik = 0
            licznikowa = random.randint(1, 3)
        elif slowo[x] in xa:
            slowo += nowylos(nowesp)
            licznik = licznik + 1
        elif slowo[x] in xb:
            slowo += nowylos(nowelitery)
    return slowo